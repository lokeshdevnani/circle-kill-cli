require_relative '../util/exceptions.rb'

class PeopleCircle
  include Enumerable

  def initialize(size)
    @people = Array.new(size, true)
    @size = size
    @index = 0
  end

  def kill_next
    @people[next_alive] = false
  end

  def pass_sword
    @index = next_alive
  end

  def person_with_sword
    return @index
  end

  def each
    @people.each_with_index do |is_alive, number|
      yield number if is_alive
    end
  end

  private

  def next_alive
    i = @index
    loop do
      i = next_index(i)
      raise NoNextAlivePersonException if i == @index
      return i if @people[i] == true
    end
  end

  def next_index(index)
    return (index - 1 + @size) % @size
  end
end