require_relative '../util/exceptions.rb'
require_relative '../models/people_circle.rb'

class CircleKillSimulator
  attr_reader :people_circle

  def initialize(args = {})
    @options = args
    @people_count = @options.delete(:people_count)
    @people_circle = PeopleCircle.new(@people_count)
  end

  def execute
    simulate
  end

  private

  def simulate
    loop do
      print_circle if @options[:simulate]
      people_circle.kill_next
      people_circle.pass_sword
    end
  rescue NoNextAlivePersonException => e
    print_circle if @options[:simulate]
    print_alive_person
  end

  def print_alive_person
    response_write "Alive Person: #{people_circle.person_with_sword}"
  end

  def print_circle
    response_write people_circle.map{|number| number}.join(" ")
  end

  def response_write(str)
    puts str
  end
end