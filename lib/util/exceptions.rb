class NoNextAlivePersonException < StandardError
  def initialize(msg = "No other person is alive")
    super(msg)
  end
end