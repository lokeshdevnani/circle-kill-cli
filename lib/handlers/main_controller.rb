require 'optionparser'
require_relative '../services/circle_kill_simulator.rb'

class MainController
  def collect_params
    options = {}
    OptionParser.new do |opts|
      opts.banner = "Usage: circle_kill -n <people_count> [options]"
      opts.on("-n", "--people_count <people_count>", Integer) do |n|
        options[:people_count] = n
      end
      opts.on("-s", "--with-simulation", "Prints step by step kills") do |n|
        options[:simulate] = true
      end
      opts.on("-c", "--with-color", "Prints colored output to the console") do |c|
        # TODO
      end
      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end
    end.parse!
    raise OptionParser::MissingArgument.new('-n, --people_count') if options[:people_count].nil?
    raise ArgumentError, 'There must be at least one person to start with' if options[:people_count] <= 0

    return options
  end

  def execute
    params = collect_params
    circle_kill_service = CircleKillSimulator.new(params)
    circle_kill_service.execute
  rescue StandardError => e
    STDERR.puts e.message
  end
end
