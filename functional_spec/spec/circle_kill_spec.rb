require 'spec_helper'

RSpec.describe 'Circle Kill CLI', type: :aruba do
  def run_cli(arguments)
    run "circle_kill #{arguments}"
  end

  context "shows number of alive person" do
    it 'works for 10 persons' do
      command = run_cli("-n 10")
      stop_all_commands
      expect(command.output).to end_with("Alive Person: 6\n")
    end

    it 'works for single person' do
      command = run_cli("-n 1")
      stop_all_commands
      expect(command.output).to end_with("Alive Person: 0\n")
    end

    it 'works for 100 persons' do
      command = run_cli("-n 100")
      stop_all_commands
      expect(command.output).to end_with("Alive Person: 28\n")
    end
  end

  context "handles invalid input" do
    it 'shows error if people_count < 1' do
      command = run_cli("-n 0")
      stop_all_commands
      expect(command.output).to end_with("There must be at least one person to start with\n")
    end

    it 'shows error if people_count argument is not passed' do
      command = run_cli("")
      stop_all_commands
      expect(command.output).to start_with("missing argument")
    end
  end

  context "--with-simulation works" do
    it 'works for 10 persons' do
      command = run_cli("-n 10 --with-simulation")
      stop_all_commands
      expected_steps = ["0 1 2 3 4 5 6 7 8 9",
                        "0 1 2 3 4 5 6 7 8",
                        "0 1 2 3 4 5 6 8",
                        "0 1 2 3 4 6 8",
                        "0 1 2 4 6 8",
                        "0 2 4 6 8",
                        "0 2 4 6",
                        "0 2 6",
                        "2 6",
                        "6"]
      expect(command.output).to start_with(expected_steps.join("\n"))
      expect(command.output).to end_with("Alive Person: 6\n")
    end

    it 'works with 2 persons' do
      command = run_cli("-n 2 -s")
      stop_all_commands
      expect(command.output).to eq("0 1\n0\nAlive Person: 0\n")
    end
  end

end
