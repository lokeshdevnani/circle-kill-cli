# Circle Kill CLI

### Installation
Clone this repo

    git clone <repo-url>

To setup the project, run `bin/setup` in project root.

### Running

Application can be started by running `bin/circle_kill` and passing appropriate arguments

Show help

    bin/circle_kill -h

Get result for Circle kill simulation for 10 people

    bin/circle_kill -n 10

Get result along with simulation, i.e. step by step status after each kill

    bin/circle_kill -n 10 --with-simulation


### Running Tests

Tests can be triggered by running:

    bin/run_functional_tests